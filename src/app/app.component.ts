import { AlterarPorteiroPage } from './../pages/alterar-porteiro/alterar-porteiro';
import { ControleVisitasPage } from './../pages/controle-visitas/controle-visitas';
import { UnidadesPage } from './../pages/unidades/unidades';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = ControleVisitasPage;
  public activeMenu: boolean = false;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Controle de Visitas', component: ControleVisitasPage , icon: 'open'},
      { title: 'Consulta de Unidades', component: UnidadesPage , icon: 'people'}
    ];

    this.pages.forEach((page) => {
      if(page.component == this.rootPage){
        this.activeMenu = page.component;
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
      this.nav.setRoot(page.component);
      this.activeMenu = page.component;
  }

  alterarPorteiro() {
    this.activeMenu = false;
      this.nav.setRoot(AlterarPorteiroPage);
  }
}
