import { CadastroPage } from './../pages/cadastro/cadastro';
import { AlterarGuaritaPage } from './../pages/alterar-guarita/alterar-guarita';
import { AlterarPorteiroPage } from './../pages/alterar-porteiro/alterar-porteiro';
import { ModalNegarPage } from './../pages/modal-negar/modal-negar';
import { ModalPageModule } from './../pages/modal/modal.module';
import { ModalPage } from './../pages/modal/modal';
import { CadastroVisitantePage } from './../pages/cadastro-visitante/cadastro-visitante';
import { BuscaDocumentoPage } from './../pages/busca-documento/busca-documento';
import { ControleVisitasPage } from './../pages/controle-visitas/controle-visitas';
import { NovoUnidadeVisitantePage } from './../pages/novo-unidade-visitante/novo-unidade-visitante';
import { LiberadosPage } from './../pages/liberados/liberados';
import { AgendadosPage } from './../pages/agendados/agendados';
import { UnidadesPage } from './../pages/unidades/unidades';
import { PrestadoresPage } from './../pages/prestadores/prestadores';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { NovoVisitantePage } from '../pages/novo-visitante/novo-visitante';
import { ApartamentoPage } from '../pages/apartamento/apartamento';
import { PrestadoresPageModule } from '../pages/prestadores/prestadores.module';
import { VisitantesPageModule } from '../pages/visitantes/visitantes.module';
import { HistoricoPageModule } from '../pages/historico/historico.module';
@NgModule({
  declarations: [
    MyApp,
    NovoVisitantePage,
    UnidadesPage,
    ApartamentoPage,
    AgendadosPage,
    LiberadosPage,
    NovoUnidadeVisitantePage,
    ControleVisitasPage,
    BuscaDocumentoPage,
    CadastroVisitantePage,
    ModalNegarPage,
    AlterarPorteiroPage,
    AlterarGuaritaPage,
    CadastroPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot(),
    VisitantesPageModule,
    HistoricoPageModule,
    PrestadoresPageModule,
    ModalPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PrestadoresPage,
    NovoVisitantePage,
    UnidadesPage,
    ApartamentoPage,
    AgendadosPage,
    LiberadosPage,
    NovoUnidadeVisitantePage,
    ControleVisitasPage,
    BuscaDocumentoPage,
    CadastroVisitantePage,
    ModalPage,
    ModalNegarPage,
    AlterarPorteiroPage,
    AlterarGuaritaPage,
    CadastroPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
