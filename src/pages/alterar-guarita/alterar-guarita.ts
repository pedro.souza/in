import { ControleVisitasPage } from './../controle-visitas/controle-visitas';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-alterar-guarita',
  templateUrl: 'alterar-guarita.html',
})
export class AlterarGuaritaPage {

  guaritas: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {

    this.guaritas = [
      {
        nome: 'Guarita 1'
      },
      {
        nome: 'Guarita 2'
      },
      {
        nome: 'Guarita 3'
      },
      {
        nome: 'Guarita 4'
      }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlterarGuaritaPage');
  }

  confirmarGuarita() {

    let loader = this.loadingCtrl.create({
      content: "Salvando alteração ...",
      duration: 2000
    });

    loader.present();

    setTimeout(() => {
      loader.dismiss();
      this.navCtrl.setRoot(ControleVisitasPage);
      let toast = this.toastCtrl.create({
        message: 'Alterado com sucesso!',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }, 3000);
  }

}
