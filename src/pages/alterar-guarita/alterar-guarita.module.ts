import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlterarGuaritaPage } from './alterar-guarita';

@NgModule({
  declarations: [
    AlterarGuaritaPage,
  ],
  imports: [
    IonicPageModule.forChild(AlterarGuaritaPage),
  ],
})
export class AlterarGuaritaPageModule {}
