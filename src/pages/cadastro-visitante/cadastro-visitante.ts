import { CadastroPage } from './../cadastro/cadastro';
import { ModalPage } from './../modal/modal';
import { ApartamentoPage } from './../apartamento/apartamento';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the CadastroVisitantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro-visitante',
  templateUrl: 'cadastro-visitante.html',
})
export class CadastroVisitantePage {

  pessoa: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroVisitantePage');
  }

  cadastrar() {
    this.navCtrl.push(CadastroPage);
  }

}
