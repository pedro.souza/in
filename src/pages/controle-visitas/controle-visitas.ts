import { NovoVisitantePage } from './../novo-visitante/novo-visitante';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ControleVisitasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-controle-visitas',
  templateUrl: 'controle-visitas.html',
})
export class ControleVisitasPage {

  abas: string = "prestadores";
  public prestadores: Array<any>;
  public btnAddVisitante: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.prestadores = [
      {
        cor: '#ff404f',
        tempo: '5min',
        img: 'https://minhabolsacheia.files.wordpress.com/2011/02/3x41.jpg',
        descricao: 'Pizza Hut',
        local: '1606 BL01',
        guarita: 'Guarita 4',
        qtdPessoas: '1 pessoa',
        hora: '23:00'
      },
      {
        cor: '#00a9c3',
        tempo: '1 hora',
        img: 'https://pbs.twimg.com/profile_images/396999721/3x4.jpg',
        descricao: 'NET',
        local: '2301 BL02',
        guarita: 'Guarita 3',
        qtdPessoas: '1 pessoa',
        hora: '23:00'
      }
    ];
  }

  novoVisitante() {
    this.navCtrl.push(NovoVisitantePage);
  }

}
