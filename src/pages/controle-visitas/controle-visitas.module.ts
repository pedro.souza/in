import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ControleVisitasPage } from './controle-visitas';

@NgModule({
  declarations: [
    ControleVisitasPage,
  ],
  imports: [
    IonicPageModule.forChild(ControleVisitasPage),
  ],
})
export class ControleVisitasPageModule {}
