import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovoVisitantePage } from './novo-visitante';

@NgModule({
  declarations: [
    NovoVisitantePage,
  ],
  imports: [
    IonicPageModule.forChild(NovoVisitantePage),
  ],
})
export class NovoVisitantePageModule {}
