import { UnidadesPage } from './../unidades/unidades';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-novo-visitante',
  templateUrl: 'novo-visitante.html',
})
export class NovoVisitantePage {

  public blocos: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log()
  }

  ionViewDidLoad(){
    this.blocos = [
      {
        bloco: 'Bloco 01'
      },
      {
        bloco: 'Bloco 02'
      },
      {
        bloco: 'Bloco 03'
      }
    ];
  }

  unidade() {
    this.navCtrl.push(UnidadesPage);
  }

}
