import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AgendadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agendados',
  templateUrl: 'agendados.html',
})
export class AgendadosPage {

  public agendados: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.agendados = [
      {
        horaEntrada: '09:00',
        horaSaida: '23:00',
        img: 'http://allmanaque.com/uploads/images/rockj.jpg',
        nome: 'Pedro Gonçalvez',
        tipo: 'Visitante'
      },
      {
        horaEntrada: '09:00',
        horaSaida: '20:00',
        img: 'http://fotologimg.s3.amazonaws.com/photo/60/63/120/ppriscilar1308/1300782958529_f.jpg',
        nome: 'Lara Pedroso',
        tipo: 'Colaborador'
      },
      {
        horaEntrada: '22:00',
        horaSaida: '23:00',
        img: 'http://www.pavan.ind.br/imagens/34.png',
        nome: 'Pizza Hut',
        tipo: 'Entregador'
      }
    ]
  }

  ionViewDidLoad() {
    
  }

}
