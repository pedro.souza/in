import { AlterarGuaritaPage } from './../alterar-guarita/alterar-guarita';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AlterarPorteiroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alterar-porteiro',
  templateUrl: 'alterar-porteiro.html',
})
export class AlterarPorteiroPage {

  selectedItem : any;
  porteiros: Array<any> = [];

  constructor(public navCtrl : NavController, public navParams : NavParams) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.porteiros = [
      {
        nome: 'Carlos de Jesus',
        matricula: '1233626262'
      },
      {
        nome: 'Roberto Firmino',
        matricula: '1233626262'
      },
      {
        nome: 'Pedro Souto',
        matricula: '1233626262'
      },
      {
        nome: 'Jair Ventura',
        matricula: '1233626262'
      }
    ];
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this
      .navCtrl
      .push(AlterarGuaritaPage, {item: item});
  }

}
