import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlterarPorteiroPage } from './alterar-porteiro';

@NgModule({
  declarations: [
    AlterarPorteiroPage,
  ],
  imports: [
    IonicPageModule.forChild(AlterarPorteiroPage),
  ],
})
export class AlterarPorteiroPageModule {}
