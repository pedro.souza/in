import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiberadosPage } from './liberados';

@NgModule({
  declarations: [
    LiberadosPage,
  ],
  imports: [
    IonicPageModule.forChild(LiberadosPage),
  ],
})
export class LiberadosPageModule {}
