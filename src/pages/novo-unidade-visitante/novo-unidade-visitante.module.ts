import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovoUnidadeVisitantePage } from './novo-unidade-visitante';

@NgModule({
  declarations: [
    NovoUnidadeVisitantePage,
  ],
  imports: [
    IonicPageModule.forChild(NovoUnidadeVisitantePage),
  ],
})
export class NovoUnidadeVisitantePageModule {}
