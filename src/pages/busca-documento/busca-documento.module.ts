import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscaDocumentoPage } from './busca-documento';

@NgModule({
  declarations: [
    BuscaDocumentoPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscaDocumentoPage),
  ],
})
export class BuscaDocumentoPageModule {}
