import { CadastroVisitantePage } from './../cadastro-visitante/cadastro-visitante';
import { ApartamentoPage } from './../apartamento/apartamento';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CadastroPage } from '../cadastro/cadastro';

/**
 * Generated class for the BuscaDocumentoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-busca-documento',
  templateUrl: 'busca-documento.html',
})
export class BuscaDocumentoPage {

  searchQuery: string = '';
  items: Array<any>;
  public temVisitante: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.temVisitante = false;
  }

  initializeItems() {
    this.items = [
      {
        nome: 'José da Silva',
        doc: '0001112229'
      },
      {
        nome: 'Pedro Hernesto',
        doc: '1111112229'
      },
      {
        nome: 'Carolina Souza',
        doc: '5551112229'
      }
    ];
  }

  getItems(ev: any) {
    this.initializeItems();

    let val = ev.target.value;    

    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return item.doc.indexOf(val) > -1 || item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1;
      })
      this.temVisitante = true;
    } else {
      this.items = [];
      this.temVisitante = false;
    }
  }

  mostrarUnidade() {
    this.navCtrl.push(CadastroPage);
  }

  cadastrarVisitante() {
    this.navCtrl.push(CadastroVisitantePage);
  }

}
