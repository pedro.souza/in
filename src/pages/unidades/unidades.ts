import { BuscaDocumentoPage } from './../busca-documento/busca-documento';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { ApartamentoPage } from '../apartamento/apartamento';
import { Item } from 'ionic-angular/components/item/item';

@IonicPage()
@Component({
  selector: 'page-unidades',
  templateUrl: 'unidades.html',
})
export class UnidadesPage {

  public unidades: Array<any>;
  public activeSearch: boolean = false;
  public searchTerm: string = '';
  public searchTermControl: FormControl;
  public noFilter: Array<any>;
  public hasFilter: boolean = false;

  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.searchTermControl = new FormControl();

      this.searchTermControl.valueChanges.debounceTime(1000).distinctUntilChanged().subscribe(search => {
        if (search !== '' && search) {
          this.filterItems();
        }
      })    
  }

  filterItems() {
    this.hasFilter = false;

    this.unidades = this.noFilter.filter((item) => {
      return item.andar.indexOf(this.searchTerm) > -1;
    });                           
  }

  ativarPesquisa() {
    this.activeSearch = true;          
  }
  
  ionViewDidLoad() {
    this.unidades = [
      {
        andar: '1º Andar',
        aps: [
          {
            img: 'https://minhabolsacheia.files.wordpress.com/2011/02/3x41.jpg',
            numero: '1101',
            bloco: 'Bloco01',
            qtdMoradores: '2 Moradores'
          },
          {
            img: 'https://openclipart.org/image/2400px/svg_to_png/277089/Female-Avatar-5.png',
            numero: '1102',
            bloco: 'Bloco01',
            qtdMoradores: '1 Morador'
          }
        ]
      },
      {
        andar: '2º Andar',
        aps: [
          {
            img: 'https://assemblive.com/assets/home_logo_avatar-32c73656536a26f3d0cb07a3a91ba524.png',
            numero: '2101',
            bloco: 'Bloco01',
            qtdMoradores: '2 Moradores'
          },
          {
            img: 'https://assemblive.com/assets/home_logo_avatar-32c73656536a26f3d0cb07a3a91ba524.png',
            numero: '2102',
            bloco: 'Bloco01',
            qtdMoradores: '1 Morador'
          }
        ]
      },
      {
        andar: '3º Andar',
        aps: [
          {
            img: 'https://assemblive.com/assets/home_logo_avatar-32c73656536a26f3d0cb07a3a91ba524.png',
            numero: '3101',
            bloco: 'Bloco01',
            qtdMoradores: '2 Moradores'
          },
          {
            img: 'https://assemblive.com/assets/home_logo_avatar-32c73656536a26f3d0cb07a3a91ba524.png',
            numero: '3102',
            bloco: 'Bloco01',
            qtdMoradores: '1 Morador'
          }
        ]
      }
    ];   

    this.noFilter = this.unidades;
    this.hasFilter = false;
  }

  mostrarAgendamentos(item){
    this.navCtrl.push(ApartamentoPage, {
      itemSelecionado: item
    });
  }
  // buscarVisitante(){
  //   this.navCtrl.push(BuscaDocumentoPage);
  // }

}
