import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NovoVisitantePage } from '../novo-visitante/novo-visitante';
@IonicPage()
@Component({
  selector: 'page-prestadores',
  templateUrl: 'prestadores.html',
})
export class PrestadoresPage {

  public prestadores: Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.prestadores = [
      {
        cor: '#ff404f',
        tempo: '5min',
        img: 'https://minhabolsacheia.files.wordpress.com/2011/02/3x41.jpg',
        descricao: 'Pizza Hut',
        local: '1606 BL01',
        qtdPessoas: '1 pessoa',
        hora: '23:00'
      },
      {
        cor: '#00a9c3',
        tempo: '1 hora',
        img: 'https://pbs.twimg.com/profile_images/396999721/3x4.jpg',
        descricao: 'NET',
        local: '2301 BL02',
        qtdPessoas: '1 pessoa',
        hora: '23:00'
      }
    ];
  }

  ionViewWillEnter(){
    this.navCtrl.parent._isToolbarVisible = true;
  }

  novoVisitante() {
    this.navCtrl.parent._isToolbarVisible = false;
    this.navCtrl.push(NovoVisitantePage);
  }

}
