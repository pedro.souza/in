import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalNegarPage } from './modal-negar';

@NgModule({
  declarations: [
    ModalNegarPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalNegarPage),
  ],
})
export class ModalNegarPageModule {}
