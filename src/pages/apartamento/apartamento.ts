import { NovoUnidadeVisitantePage } from './../novo-unidade-visitante/novo-unidade-visitante';
import { UnidadesPage } from './../unidades/unidades';
import { LiberadosPage } from './../liberados/liberados';
import { AgendadosPage } from './../agendados/agendados';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PrestadoresPage } from '../prestadores/prestadores';
import { VisitantesPage } from '../visitantes/visitantes';
import { HistoricoPage } from '../historico/historico';
import { ModalPage } from '../modal/modal';
import { CadastroPage } from '../cadastro/cadastro';
import { BuscaDocumentoPage } from '../busca-documento/busca-documento';

@IonicPage()
@Component({
  selector: 'page-apartamento',
  templateUrl: 'apartamento.html',
})
export class ApartamentoPage {

  public unidade: any;
  public ap: any;
  public bloco: any;

  public agendados: Array<any> = [];

  abas: string = "agendados";

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController) {
    this.unidade = this.navParams.get('itemSelecionado');
    this.ap = this.unidade.numero;
    this.bloco = this.unidade.bloco;
  }

  ionViewDidLoad() {
    this.agendados = [
      {
        horaEntrada: '09:00',
        horaSaida: '23:00',
        img: 'http://allmanaque.com/uploads/images/rockj.jpg',
        nome: 'Pedro Gonçalvez',
        tipo: 'Visitante'
      },
      {
        horaEntrada: '09:00',
        horaSaida: '20:00',
        img: 'http://fotologimg.s3.amazonaws.com/photo/60/63/120/ppriscilar1308/1300782958529_f.jpg',
        nome: 'Lara Pedroso',
        tipo: 'Colaborador'
      },
      {
        horaEntrada: '22:00',
        horaSaida: '23:00',
        img: 'http://www.pavan.ind.br/imagens/34.png',
        nome: 'Pizza Hut',
        tipo: 'Entregador'
      }
    ];
  }

  openModal(){
    let modal = this.modalCtrl.create(ModalPage);
    modal.present();
  }

  agendarVisita() {
    this.navCtrl.push(BuscaDocumentoPage);
  }
}
