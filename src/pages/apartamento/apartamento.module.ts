import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApartamentoPage } from './apartamento';

@NgModule({
  declarations: [
    ApartamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(ApartamentoPage),
  ],
})
export class ApartamentoPageModule {}
